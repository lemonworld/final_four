% Template for ICIP-2014 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf,amsmath,graphicx, amsfonts}

% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\L{{\cal L}}

% Title.
% ------
\title{Alpha Go}
%
% Single address.
% ---------------
\name{Marcel Wienöbst}
\address{Universität zu Lübeck}
%
% For example:
% ------------
%\address{School\\
%	Department\\
%	Address}
%
% Two addresses (uncomment and modify for two-address case).
% ----------------------------------------------------------
%\twoauthors
%  {A. Author-one, B. Author-two\sthanks{Thanks to XYZ agency for funding.}}
%	{School A-B\\
%	Department A-B\\
%	Address A-B}
%  {C. Author-three, D. Author-four\sthanks{The fourth author performed the work
%	while at ...}}
%	{School C-D\\
%	Department C-D\\
%	Address C-D}
%

\DeclareMathOperator*{\argmax}{arg\,max}

\begin{document}
%\ninept
%
\maketitle
%
\begin{abstract}
  Das Brettspiel Go galt lange Zeit als unüberwindbare Hürde für künstliche Intelligenzen der heutigen Zeit. Durch die Verwendung von \emph{Deep Neural Networks} gelang es den Forschern von DeepMind jedoch diese Hürde zu überwinden und zum ersten Mal mit dem Programm Alpha Go~\cite{ag} einen professionellen Go-Spieler zu besiegen. Dies gelang im Oktober 2015 gegen Europameister Fan Hui. In dieser Seminararbeit wird das genutzte Verfahren vorgestellt und gezeigt, wie dieser Ansatz durch Alpha Go Zero im Oktober 2017~\cite{agz} noch verbessert wurde.
\end{abstract}
%
%\begin{keywords}
%One, two, three, four, five
%\end{keywords}
%
\section{Einführung}
\label{sec:intro}

Brettspiele wie Dame, Schach und Go sind von jeher Spielfelder für Entwickler von künstlichen Intelligenzen. Insbesondere das asiatische Spiel Go, schien dabei für lange Zeit eine unüberwindbare Hürde. Das Spiel wird auf einem $19 \times 19$ Brett gespielt. Die Spieler setzen dabei abwechselnde Steine auf das Brett mit dem Ziel einerseits möglichst viele Felder zu umschließen und andererseits gegnerische Steine gefangen zu nehmen. Es ist zunächst wichtig zu verstehen, warum klassiche künstliche Intelligenzen Schwierigkeiten haben, menschliche Stärke beim Go-Spiel zu erreichen.  Dazu betrachten wir die Komplexität eines Spiels mit vollständiger Information. Diese Komplexität kann durch die Größe des Baumes abgeschätzt werden, der bei einer vollständigen Suche aller möglichen Zugabfolgen traversiert wird. Kann dieser Baum in annehmbarer Zeit durchlaufen werden, so kann das Spiel praktisch gelöst werden, indem jedem Knoten (also jeder Position $s$) rekursiv der Spielausgang $v^*(s)$ bei perfektem Spiel zugeordnet wird. Für viele Spiele (darunter Schach und Go) ist die vollständige Erforschung dieses Baumes jedoch zu aufwändig. Dies kann mit folgender Berechnung verdeutlicht werden. Gibt es pro Position durchschnittlich $b$-viele legale Züge und dauert eine Partie durchschnittlich $d$ Züge, so kann die Größe des Baumes mit $b^d$ abgeschätzt werden. Für das Schachspiel ergeben sich  damit $35^{80}$ und für Go $250^{150}$ Knoten. Es müssen also andere Ansätze genutzt werden.

Alle derzeitigen Verfahren (und auch Alpha Go) approximieren nun die Funktion $v^*(s)$, indem die Breite und die Tiefe des Baumes reduziert werden. Die Tiefe kann reduziert werden, indem der Baum bis zu einer Maximaltiefe vollständig erkundet wird und allen \emph{Blättern} mithilfe von Heuristiken bewertet werden. Mit diesem Ansatz sind die gängigen Schachprogramme stärker als die besten menschlichen Spieler. Beim Go-Spiel führt dieser Ansatz jedoch nicht zum Erfolg, da die Komplexität deutlich höher ist. Darüber hinaus ist es schwieriger durch Heuristiken eine Stellung korrekt zu evaluieren. Es muss also auch Die Breite des Baumes reduziert werden, indem nicht alle legalen Züge betracht werden. Ein Ansatz, der von Go-Programmen häufig verwendet wird, sind sogenannte Monte Carlo Rollouts. Bei diesen werden Züge nach einer \emph{Policy} $p$ vorgegeben und von einer Position aus ohne Verzweigungen bis zum Spielende simuliert. Diese Policy ordnet einer Position eine Wahrscheinlichkeitsverteilung über die legalen Züge zu. Bei einem Monte Carlo Rollout wird bei jeder Zugauswahl der Zug mit der höchsten Wahrscheinlichkeit gewählt. Dieser Ansatz wird dabei für die Monte Carlo Tree Search (MCTS) genutzt, die im Detail in späteren Abschnitten diskutiert werden. Das Grundprinzip ist das Folgende: Es werden eine Reihe von Monte Carlo Rollouts durchgeführt und die Policy sowie Stellungsbewertungen je nach Ergebnis des Rollouts angepasst. Es kann gezeigt werden, dass dann die Positionsbewertung mit steigender Anzahl von Rollouts gegen $v^*(s)$ konvergiert.

\section{Alpha Go}
\label{sec:alphago}
Der Ansatz von Alpha Go ist es, die Policy und die Stellungsbewertungen mithilfe von neuronalen Netzen zu lernen, um eine effektivere Monte Carlo Tree Search durchzuführen. Im Kern werden dazu verschiedene Policy{-} und ein Bewertungsnetz trainiert.

\begin{figure}
  \includegraphics[width = 0.5\textwidth]{networks.png}
  \caption{Die vier für die MCTS trainierten Netze}
  \label{ntws}
\end{figure}

Die trainierten Netze sind in Abbildung~\ref{ntws} zu sehen und werden im Folgenden vorgestellt. Anschließend wird genauer auf die Umsetzung der Monte Carlo Tree Search eingegangen.

Zunächst wird durch überwachtes Lernen das SL Policy Netzwerk trainiert. Dieses besteht aus 13 Schichten: abwechselnd Convolutional Layers und Rectifier Nonlinearities mit einer abschließenden Softmax-Layer. Trainiert wird auf 30 Mio{.} Positionen aus einer Partiedatenbank von Profispielern, das Netz lernt also menschliche Züge vorherzusagen. Die Eingabe ist also die Repräsentation einer Go-Position. Für das Training wird ein Gradientenabstieg genutzt, der den Vorhersagefehler minimiert:
\[
  \Delta \sigma \propto \frac{\partial \log p_{\sigma}(a | s)}{\partial \sigma}
\]
Hier ist $p_{\sigma}$ die SL Policy und $p(a | s)$ die Wahrscheinlichkeit von Zug $a$ in Position $s$.
Durch die Nutzung von neuronalen Netzen konnte die Genauigkeit der Vorhersage im Vergleich zu vorigen Arbeiten signifikant erhöht werden, es werden 57\% der Züge korrekt erkannt. Zusätzlich wird auch ein kleineres Netz $p_{\pi}$ auf den gleichen Daten trainiert, dass für die Monte Carlo Rollouts möglichst schnell Züge vorhersagt.

Die bisherigen Netze sind darauf trainiert den Fehler bei der Vorhersage von menschlichen Zügen zu minimieren. Bei der Zugauswahl eines Go-Programms sollte jedoch der Zug gewählt werden, der das Partieresultat optimiert (und damit der beste Zug ist). Daher wird ausgehend vom vorherigen Netz durch verstärkendes Lernen ein weiteres Policy Netzwerk trainiert, welches den Zug im Hinblick auf den Spielausgang optimiert. Dazu spielt das aktuelle Policy Netzwerk gegen eine zufällige (um Overfitting zu verhindern) alte Version. Auch hier wird Gradientenabstieg genutzt, in die Richtung, welche das erwartete Partieresultat maximiert:
\[
  \Delta \rho \propto \frac{\partial \log p_{\rho}(a_t | s_t)}{\partial \rho} z_t
\]
Dieses trainierte Netz war bereits in der Lage 85\% der Spiele gegen das schwächere Go-Programm Pachi zu gewinnen und dies ohne ein Suchverfahren zu nutzen.

Die nach diesen Überlegungen trainierten Netze geben jeweils eine Wahrscheinlichkeitsverteilung über die legalen Züge aus. Für die Monte Carlo Tree Search wird darüber hinaus noch eine Stellungsbewertung benötigt. Dazu wird ein Bewertungsnetz nach gleichem Schema wie die Policy Netzwerke (abgesehen von der Ausgabe) trainiert. Dieses soll die in der Einführung vorgestellte Funktion $v^*(s)$ approximieren. Dazu wird per Regression auf Datenpaaren bestehend aus Position und Spielresultat gelernt. Der quadratische Fehler zwischen soll per Gradientenabstieg minimiert werden:
\[
  v^p(s) = \mathbb{E}[z_t | s_t = s, a_{t \dots T} \approx p]
\]
Um Overfitting zu verhindern, wurden 30 Mio{.} Postionen genutzt, wobei jede Position aus einem anderen Spiel stammt. Diese Spiele wurden vom Policy Netzwerk generiert, indem es gegen sich selbst gespielt hat. 

Wir betrachten nun wie die Monte Carlo Tree Search mithilfe der trainierten Netze durchgeführt werden kann.
\begin{figure}
  \includegraphics[width = 0.5\textwidth]{mcts_go.png}
  \caption{Die Monte Carlo Tree Search bei Alpha Go}
  \label{mcts_go}
\end{figure}
Eine schematische Darstellung ist in Abbildung~\ref{mcts_go} zu sehen. Die Monte Carlo Tree Search besteht aus einer Reihe von Simulationen. Dabei wird der Baum bis zu einem Blatt traversiert, dieses per Monte Carlo Rollout und mit dem Bewertungsnetz evaluiert und die entsprechenden Bewertungen (Action Values) und Zähler aktualisiert. Wir schauen uns dies nun im Detail an:

In jeder Kante $(s,a)$ (Position, Zug) des Suchbaums wird ein Action Value $Q(s,a)$, ein Zähler der Besuchshäufigkeit $N(s,a)$ und die a-priori Wahrscheinlichkeit $P(s,a)$ gespeichert. Der Baum wird durch wiederholte Simulationen ausgehend von der Wurzel erkundet. Dabei wird in Zeitschritt $t$ der folgende Zug gewählt:
\[
  a_t = \argmax_a(Q(s_t,a)+u(s_t,a))
\]
Es wird also das Maximum von $Q(s_t,a)$ gesucht, kombiniert mit dem additiven Term $u(s,a)$. Dieser ist folgendermaßen definiert
\[
  u(s,a) \propto \frac{P(s,a)}{1+N(s,a)},
\]
also proportional zu der a-priori Wahrscheinlichkeit, aber abnehmend mit zunehmender Besuchshäufigkeit, um eine Erkundung des Baumes zu unterstützen. Die a-priori Wahrscheinlichkeiten sind die Ausgaben des Policy Netzwerks. Jede Simulation erreicht schließlich einen Blattknoten nach $L$ Schritten. Diese Position wird nun bewertet und bildet damit eine Bewertung der gesamten Simulation. Dazu wird einerseits ein Monte Carlo rollout bis zur Terminierung durchgeführt, wobei das schnelle Policy Netzwerk genutzt wird. Das Resultat wird in $z_L$ gespeichert. Andererseits wird die Position mit dem Bewertungsnetz eingeschätzt ($v_{\theta}(s_L)$). Die Bewertung des Blattes $V(s_L)$ dann:
\[
  V(s_L) = (1-\lambda)v_{\Theta} + \lambda z_L
\]
Die beiden Bewertungen werden also mit dem Parameter $\lambda$ kombiniert. Die Action Values $Q(s, a)$ sowie die Besuchshäufigkeiten $N(s,a)$ werden nach jeder Simulation aktualisiert. Die Besuchshäufigkeiten eines Knotens werden aufaddiert und die Bewertungen gemittelt:
\begin{align*}
  N(s,a) &= \sum_{i=1}^n 1(s,a,i) \\
  Q(s,a) &= \frac{1}{N(s,a)} \sum_{i=1}^n1(s,a,i)V(s_L^i)
\end{align*}
Dabei ist $s_L^i$ der Blattknoten der $i$-ten Simulation und $1(s,a,i)$ gibt an, ob eine Kante $(s,a)$ während der $i$-ten Simulation traversiert wird.

Mit dieser Methodik gelang es nun den Forschern von DeepMind ein Go-Programm zu entwickeln, dass den bisherigen weit überlegen ist. In einem Turnier von verschiedenen Programmen gewann Alpha Go 494 von 495 Spielen. Darüber hinaus wurde zum ersten Mal ein professioneller menschlicher Spieler von einem Computerprogramm besiegt. Der europäische Meister Fan Hui wurde 5 zu 0 besiegt. Dies war ein Durchbruch der weltweit auch außerhalb der Forscher- und Go-Gemeinde für großes Aufsehen gesorgt hat.

\section{Alpha Go Zero}
\label{sec:zero}
Nach dem durchschlagenden Erfolg von Alpha Go haben die Forscher von DeepMind versucht die Methodik weiter zu verbessern. Im Oktober 2017 wurde schließlich Alpha Go Zero vorgestellt. Dabei wurde vollständig auf die Nutzung von menschlichem Wissen verzichtet. Bei Alpha Go hingegen wurden das Policy- und das Bewertungsnetz mithilfe einer Datenbank von Go-Spielen trainiert. Darüber hinaus besteht Alpha Go Zero nur noch aus einem neuronalem Netz, während Alpha Go aus vier verschiedenen Netzen aufgebaut war. Dieses Netz bekommt als Eingabe die Repräsentation einer Positon und gibt eine Wahrscheinlichkeitsverteilung über die legalen Züge, welche angibt mit welcher Wahrscheinlichkeit ein Zug der beste ist, sowie eine Bewertung aus. Damit ist dieses Netz sowohl Policy als auch Bewertungsnetzwerk. Das Netz besteht aus abwechselnden Schichten von Convolutional Layers, Batch Normalisierungen und Rectifier Nonlinearities.
\begin{figure}
  \includegraphics[width = 0.5\textwidth]{agzero_selfplay.png}
  \caption{Das neue Lernverfahren bei Alpha Go Zero}
  \label{agzero_selfplay}
\end{figure}
Das Training läuft wie in Abbildung~\ref{agzero_selfplay} skizziert ab. Das Netz spielt nach dem Prinzip der Monte Carlo Tree Search eine Partie mit sich selbst. Dies kann prinzipiell genau wie zuvor durchgeführt werden. Eine Änderung liegt im Verzicht auf das kleinere, schnelle Policy Netzwerk, welches für die Blattbewertung ein Monte Carlo Rollout durchgeführt hat. Stattdessen wird hier ausschließlich die Bewertung des Netzwerks genutzt. Im nächsten Schritt passt nun das Netzwerk die Parameter so an, dass die Wahrscheinlichkeitsverteilung näher an dem gespielten Zug und die Bewertung näher am Ergebnis des Spiels ist. Es wird also verstärkend gelernt, wobei diese Verstärkung gerade durch die Monte Carlo Tree Search möglich gemacht wird, die die Policy und die Bewertung verbessert. Die Monte Carlo Tree Search, die zum Training genutzt wird, kann in der gleichen Form beim Spiel gegen menschliche Gegner oder andere künstliche Intelligenzen genutzt werden.

Auf diese Art und Weise hat das Netz 4{,}9 Mio{.} Spiele gegen sich selbst gespielt und wurde währenddessen trainiert. Dieses Netzwerk war bereits nach 36h Training der besten vorherigen Version überlegen. Die Version Alpha Go Lee wurde nach Abschluss des Trainings mit 100 zu 0 geschlagen. Nicht nur ist Alpha Go Zero deutlich stärker als alle bisherigen Ansätze, darüber hinaus ist bereits das Netzwerk ohne Monte Carlo Tree Search stärker als die bisher beste künstliche Intelligenz Crazy Stone. Die Stärke dieses eleganten und simplen Verfahrens im Gegensatz zu vorigen Programmen von DeepMind wirft die Frage auf, wie diese Diskrepanz erklärbar ist. 

\begin{figure}
  \includegraphics[width = 0.5\textwidth]{agzero_limits.png}
  \caption{Alpha Go Zero im Vergleich zu überwachtem Lernen}
  \label{agzero_limits}
\end{figure}

Eine Antwort darauf gibt Abbildung~\ref{agzero_limits}. Dort wird der Ansatz von Alpha Go Zero mit dem überwachtem Lernen von Alpha Go verglichen. Zunächst wird in Teilgraphik a deutlich, dass das überwachte Lernen zunächst überlegen ist. Nach ungefähr 25h Lernzeit überholt der Ansatz von Alpha Go Zero dieses Verfahren. Außerdem fällt auf, dass mit überwachtem Lernen die Züge von starken menschlichen Spielern besser vorhergesagt werden können (in Teilgraphik b dargestellt). Dies ist keine Überraschung, da schließlich auf einer Datenbank von Go-Spielen trainiert wurde. Beim überwachten Lernen scheint es jedoch nicht möglich sich weit über das Niveau der Menschen hinaus zu verbessern. Man könnte sogar sagen, dass dieses Wissen sich letztendlich als Hindernis darstellt. Aus historischer Sicht ist dieses Phänomen verständlich. Als Alpha Go entwickelt wurde, wollte man sich an der menschlichen Leistung orientieren. Alpha Go Zero zeigt nun, dass dies nicht nötig ist. In Teilgraphik c erkennt man klar das bessere Spielverständnis von Alpha Go Zero. Dieses ist deutlich besser in der Lage, dass Spielergebnis vorherzusagen, das heißt die Funktion $v^*(s)$ wird genauer approximiert.

\section{Fazit und Ausblick}
\label{sec:future}
Die Fortschritte, die durch Alpha Go und Alpha Go Zero, gemacht wurden, haben nicht nur die Go-Welt nachhaltig verändert. Noch im Jahr 2015 schien das Go-Spiel für künstliche Intelligenzen eine unüberwindbare Hürde. Doch nach den Siegen gegen die weltbesten Go-Spieler, entwickelte DeepMind im Oktober 2017 ein wiederum deutlich stärkeres Programm.
Dies wurde in erster Linie durch die Renaissance der neuronalen Netze und insbesondere durch die deep neural networks ermöglicht. 
Insbesondere der sehr allgemeine Ansatz von Alpha Go Zero verspricht in vielen weiteren Gebieten übermenschliche Performance bzw. deutliche Verbesserungen gegenüber den bisher besten Computerprogrammen und zeigt, dass menschliche Performance nur ein Zwischenschritt sind. 
\begin{figure}
  \includegraphics[width = 0.5\textwidth]{zero_comp.png}
  \caption{Alpha Zero - ein Verfahren für verschiedene Spiele}
  \label{zero_comp}
\end{figure}
Ein interessantes Beispiel dafür liefern die Forscher von DeepMind selbst. Das für Alpha Go Zero genutzte Verfahren wurde auf das Schachspiel und chinesisches Schach übertragen~\cite{acz} und besiegte dort das bisher beste Programm Stockfish mit 28 zu 0 Siegen bei 72 Remis (siehe Abbildung~\ref{zero_comp}). Es bleibt abzuwarten, welche weiteren Probleme mit dieser Methode gelöst werden können.


% References should be produced using the bibtex program from suitable
% BiBTeX files (here: refs). The IEEEbib.bst bibliography
% style file from IEEE produces unsorted bibliography list.
% -------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\bibliography{refs}

\end{document}
