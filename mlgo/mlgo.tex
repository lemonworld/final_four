\documentclass[ngerman]{beamer}
\geometry{paperwidth=140mm,paperheight=105mm}
% pdflatex
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}

\usepackage{graphicx}
\usepackage{tabularx,booktabs} 
\usepackage{animate}

%\usepackage{siunitx}
\usepackage{url}
\usepackage{hyperref}
\usetheme{NewLuebeck}

\usepackage{pgf,tikz}
\usetikzlibrary{circuits.logic.IEC}
\usetikzlibrary{shapes,patterns,arrows,arrows.meta,positioning,calc,matrix}
\tikzset{%
boxed/.style={draw=structure.fg, thick, fill = structure.bg},
state/.style={boxed, circle},
gate/.style={boxed, rectangle, minimum size},
graph/.style={semithick, >={Stealth[round, sep]}},
accepting/.style={double},
treenode/.style = {align=center, inner sep=3pt, text centered, draw=structure.fg, circle, thick, fill = structure.bg},
greennode/.style = {align=center, inner sep=3pt, text centered, draw=block title example.fg, circle, thick, fill = block title example.bg},
rednode/.style = {align=center, inner sep=3pt, text centered, draw=red, circle, thick, fill = red!20},
purplenode/.style = {align=center, inner sep=3pt, text centered, draw=purple, circle, thick, fill = purple!20},
bluenode/.style = {align=center, inner sep=3pt, text centered, draw=blue, circle, thick, fill = blue!20}
}

\usepackage{amsmath}
\usepackage{amsthm}
\theoremstyle{remark}

\usepackage{breqn}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

%Figures
\usepackage{subfigure}
\usepackage[export]{adjustbox}% http://ctan.org/pkg/adjustbox

\newcommand{\otime}{\mathcal{O}}
\newcommand{\blue}{\usebeamercolor[structure.fg]{normal text}}

\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

%\AtBeginSection[]
% {
%   \begin{frame}<beamer>
%     \frametitle{Gliederung}
%     \tableofcontents[currentsection]
%   \end{frame}
% }
\DeclareMathOperator*{\argmax}{arg\,max}
\title{Alpha Go}

\author[Marcel Wienöbst: Alpha Go]{Marcel Wienöbst}
\date[27. Juni 2018]{Seminar \textit{Maschinelles Lernen}}
\begin{document}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}

\frame{\titlepage}

%\begin{frame}{Gliederung}
%  \tableofcontents
%\end{frame}

\begin{frame}{Alpha Go schlägt Fan Hui}
  \begin{figure}[!tbp]
  \centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{Nature-Go-game.jpg}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{docu.jpg}
  \end{minipage}
\end{figure}
\end{frame}

\begin{frame}{Go}
  \begin{itemize}
  \item 19 $\times$ 19 Spielfeld auf das die Spieler abwechselnd Steine legen.
  \item Wie z.B. Schach ein Spiel mit vollständiger Information.
  \item Hohe Komplexität, daher sind klassische KI-Methoden dem Menschen weit unterlegen.
  \end{itemize}
\end{frame}

\begin{frame}{Go}
    \includegraphics[width= 0.6\textwidth]{go.png}
\end{frame}

\begin{frame}{Bisherige Ansätze}
  \begin{itemize}
  \item $v^* (s)$ beschreibt das Resultat ausgehend vom Brettzustand $s$ unter perfektem Spiel.
  \item Eine rekursive Berechnung ist aufgrund der großen Anzahl von erreichbaren Zuständen ($\approx 250^{150}$) nicht durchführbar.
  \item Lösung 1: reduziere Tiefe des Rekursionsbaums und approximiere $v^*(s)$
  \item Lösung 2: reduziere die Breite des Rekursionsbaums (Monte Carlo Rollouts)
  \end{itemize}
\end{frame}
\begin{frame}{Bisherige Ansätze}
  \begin{itemize}
  \item Lösung 3: Monte Carlo Tree Search (MCTS)
  \item Simuliere große Anzahl von Spielen und passe dabei die Zugauswahl und Evaluationen an.
  \item Es kann gezeigt werden, dass die Evaluation gegen $v^*(s)$ konvergiert.
  \end{itemize}
  \includegraphics[width=0.9\textwidth]{mcts.png}
\end{frame}

\begin{frame}{Alpha Go}
  \begin{itemize}
  \item Alpha Go setzt die Monte Carlo Tree Search mit Deep Networks um.
  \item Dazu werden Policy und Value Networks benötigt.
  \end{itemize}
  \begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{networks.png}
  \end{figure}
\end{frame}

\begin{frame}{Überwachtes Lernen des Policy Networks}
  \begin{itemize}
  \item Ansatz: Lerne die Züge von starken menschlichen Spielern vorauszusagen.
  \item Eingabe: Brettrepräsentation
  \item Netz: Convolutional Layers und ReLUs gefolgt von Softmax Layer, die WS-Verteilung ausgibt
  \item Gradientenabstieg:
    \[
      \Delta \sigma \propto \frac{\partial \log p_{\sigma}(a | s)}{\partial \sigma}
    \]
  \item Nach gleichem Verfahren wird das kleinere Netz $p_{\pi}$ trainiert.
  \end{itemize}
\end{frame}

\begin{frame}{Verstärkendes Lernen des Policy Networks}
  \begin{itemize}
  \item Problem: Bisher nur Optimierung der Zugvorhersage, nicht des Spielausgangs.
  \item Lösung: Das Policy Network spielt gegen alte Versionen von sich selbst und passt sich je nach Resultat $z_t$ an.
  \item Gradientenabstieg:
    \[
      \Delta \rho \propto \frac{\partial \log p_{\rho}(a_t | s_t)}{\partial \rho} z_t
    \]
  \end{itemize}
\end{frame}

\begin{frame}{Überwachtes Lernen des Value Networks}
  \begin{itemize}
  \item Regression: Lerne Partieresultat zu Stellung, wenn die Spieler Policy $p$ nutzen:
    \[
      v^p(s) = \mathbb{E}[z_t | s_t = s, a_{t \dots T} \approx p]
    \]
  \item Idee: $v^p(s)$ ist bei starker Policy ähnlich zu $v^*(s)$.
  \item Daten: Es werden 30 Mio. Spiele generiert und um Overfitting zu verhindern nur eine (zufällige) Position pro Spiel verwendet
  \item Gradientenabstieg:
    \[
      \Delta \theta \propto \frac{\partial v_{\theta}(s)}{\partial \theta}(z - v_{\theta}(s))
    \]
  \end{itemize}
\end{frame}

\begin{frame}{Evaluierung der Netzwerke}
  \includegraphics[width = \textwidth]{kgs_learning.png}
\end{frame}

\begin{frame}{MCTS mit Policy und Value Network}
  \begin{itemize}
  \item Speichere für jede Kante $(s,a)$ den Action Value $Q(s,a)$, einen Besuchszähler $N(s,a)$ und die a-priori Wahrscheinlichkeit $P(s,a)$.
  \item Auswahl des $t$-ten Zuges:
    \[
      a_t  = \argmax_a(Q(s_t,a)+u(s_t,a))
    \]
  \item $u(s, a)$ ist definiert als
    \[
      u(s,a) \propto \frac{P(s,a)}{1+N(s,a)}
    \]
  \end{itemize}
\end{frame}

\begin{frame}{MCTS mit Policy und Value Network}
  \begin{itemize}
  \item Blätter werden auf zwei Arten evaluiert:
    \begin{enumerate}
    \item Durch das Value Network: $v_\theta(s_L)$
    \item Durch einen Monte Carlo Rollout mit der Policy $p_{\pi}$ bis zum Resultat $z_L$
    \end{enumerate}
    \item Die  Bewertungen werden dann durch den Parameter $\lambda$ kombiniert:
    \[
      V(s_L) = (1 - \lambda) v_{\theta}(S_L) + \lambda z_L
    \]
  \item $N(s,a)$ und $Q(s,a)$ werden nach jeder Simulation aktualisiert:
    \begin{align*}
  N(s,a) &= \sum_{i=1}^n 1(s,a,i) \\
  Q(s,a) &= \frac{1}{N(s,a)} \sum_{i=1}^n1(s,a,i)V(s_L^i)
\end{align*}
  \end{itemize}
\end{frame}

\begin{frame}{MCTS mit Policy und Value Netzwerk}
  \begin{figure}
    \centering
    \includegraphics[width = \textwidth]{mcts_go.png}
  \end{figure}
\end{frame}

\begin{frame}{Wie gut ist Alpha Go?}
  \includegraphics[width = \textwidth]{ag_elo.png}
\end{frame}

\begin{frame}{Alpha Go Zero}
  \begin{itemize}
  \item Wird durch verstärkendes Lernen und vollständig ohne menschliches Wissen trainiert
  \item Nur noch ein Netzwerk, dass sowohl die Policy als auch eine Stellungsbewertung ausgibt
  \item Das Netz besteht aus abwechselnden Schichten von Convolutional Layers, Batch Normalizations und ReLUs.
  \end{itemize}
\end{frame}

\begin{frame}{Verstärkendes Lernen bei Alpha Go Zero}
  \begin{itemize}
  \item Idee: Nutze MCTS als Verstärkungsoperator.
  \item Das Netzwerk spielt mit MCTS gegen sich selbst und speichert Wahrscheinlichkeiten, Züge und das Resultat.
  \item Danach wird die Stellungsbewertung an das Resultat und die Policy an die Wahrscheinlichkeiten der Suche angepasst.
  \end{itemize}
\end{frame}

\begin{frame}{Verstärkendes Lernen bei Alpha Go Zero}
  \includegraphics[width=0.7\textwidth]{agzero_selfplay.png}
\end{frame}

\begin{frame}{MCTS bei Alpha Go Zero}
  \begin{itemize}
  \item MCTS wird wie zuvor genutzt
  \item Unterschied: Kein Rollout zur Blattevaluierung, sondern nur die Stellungsbewertung des Netzes
  \end{itemize}
  \includegraphics[width = \textwidth]{agzero_mcts.png}
\end{frame}

\begin{frame}{Vergleich mit überwachtem Lernen}
  \includegraphics[width = \textwidth]{agzero_limits.png}
\end{frame}

\begin{frame}{Wie gut ist Alpha Go Zero?}
  \includegraphics[width = \textwidth]{agzero_elo.png}
\end{frame}

\begin{frame}{Ausblick}
  \begin{itemize}
  \item Großer Durchbruch im Bereich KI
  \item Anwendung des Alpha Go Zero Algorithmus für andere Probleme
  \end{itemize}
  \includegraphics[width = \textwidth]{zero_comp.png}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

% \begin{frame}{Quellen}
%   \begin{itemize}
%   \item Parallelverarbeitung Skript 2015, Kapitel 13
%   \end{itemize}
% \end{frame}
  
%\input{tikz/automat.tikz}


\end{document}
