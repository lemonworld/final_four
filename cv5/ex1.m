load('ultrasound.mat');
figure(1);
subplot(2, 1, 1), plot(1:size(X,2), X);
subplot(2, 1, 2), plot(1:size(Y,2), Y);
Z = ones(1, 201);
mx = 0;
mxi = -1;
for i=-100:100
    Z(101+i) = cross_correlation(X, Y, i);
    if Z(101+i) > mx
        mx = Z(101+i);
        mxi = i;
    end
end
figure(2);
plot(-100:100, Z);
mxi