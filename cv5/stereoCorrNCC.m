function [dispMap] = stereoCorrNCC(left, right, winSize, maxDisp)

[m,n] = size(left);

dispMap = zeros(m,n);

for i = 1:m
    for j = 1:n
        mx = 0;
        mxi = 0;
        for k = 0:maxDisp
            sum = 0;
            d1 = 0;
            d2 = 0;
            for l = -floor(winSize/2):floor(winSize/2)
                for o = -floor(winSize/2):floor(winSize/2)
                    if (i + l > 0 && i + l <= m) && (j + o > 0 && j ...
                                                     + o <= n) && ...
                            (j + o - k > 0 && j + o - k <= n)
                        sum = sum + (left(i+l, j+o)*right(i + l, ...
                                                            j + o - ...
                                                            k));
                        d1 = d1 + left(i+l, j+o)^2;
                        d2 = d2 + right(i+l, j+o-k)^2;
                    end
                end
            end
            sum = sum / (sqrt(d1*d2));
            if mx < sum
                mx = sum;
                mxi = k;
            end
        end
        dispMap(i,j) = mxi;
    end 
end