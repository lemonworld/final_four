left = double(imread('left.jpg'));
right = double(imread('right.jpg'));

figure(1);
imagesc(left);
figure(2);
imagesc(right);
figure(3);
imagesc(stereoCorr(left, right, 9, 16));
figure(4);
imagesc(stereoCorrNCC(left, right, 9, 16));