function [rho] = cross_correlation(X, Y, t)

m = size(X,2);
n = size(Y,2);
mx = mean(X);
my = mean(Y);
rho = 0;
for i = max(1, 1-t):min(m, n-t) 
    rho = rho + (X(i) - mx)*(Y(i+t) - my);
end
xsum = 0;
ysum = 0;
for i = max(1, 1-t):min(m, n-t)
    xsum = xsum + (X(i) - mx)^2;
    ysum = ysum + (Y(i+t) - my)^2;
end
rho = rho / (sqrt(xsum)*sqrt(ysum));