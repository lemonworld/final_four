function [dispMap] = stereoCorr(left, right, winSize, maxDisp)

[m,n] = size(left);

dispMap = zeros(m,n);

for i = 1:m
    for j = 1:n
        mn = 10^6;
        mni = 0;
        for k = 0:maxDisp
            sum = 0;
            for l = -floor(winSize/2):floor(winSize/2)
                for o = -floor(winSize/2):floor(winSize/2)
                    if (i + l > 0 && i + l <= m) && (j + o > 0 && j ...
                                                     + o <= n) && ...
                            (j + o - k > 0 && j + o - k <= n)
                        sum = sum + (left(i+l, j+o) - right(i + l, ...
                                                            j + o - ...
                                                            k))^2;
                    end
                end
            end
            if mn > sum
                mn = sum;
                mni = k;
            end
        end
        dispMap(i,j) = mni;
    end 
end