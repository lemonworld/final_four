function plotMarks(map,varargin)
%plot red marks to an image
%   plotMarks(map) plots marks at the positions of the non zero entries in 
%   the map. In case an image has been drawn in the current figure the
%   marks will be put on top.
%
%   plotMarks(map,h) using h a figure handle can be specified.

if nargin>1
    figure(varargin{1});
end

[col, row]=find(map);
hold on
plot(row, col, 'r.')
hold off