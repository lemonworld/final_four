% CV - Exercise Sheet 3 
% Exercise 3.3 - Key point detection

% Key point detectors: 
% --> local maxima of K 
% --> local maxima of K-alpha*H^2 

% Load data
load('ueb331.mat')
Im2 = im2double(imread('ueb332.jpg'));

% -------------------------------------------------------------------------
% Implement a function keyPointsStructureTensor(image, alpha)
% which computes K and K-alpha*H^2 for a given image 
% K and H are the invariants of the structure tensor J 
alpha=0.1;

thresh = 10^-6;

[K, KmaH]   = keyPointsStructureTensor(Im, alpha); 
[K2, KmaH2] = keyPointsStructureTensor(Im2, alpha);
% -------------------------------------------------------------------------

% Find local maxima of K and K-alpha*H^2
map_K = findLocalMax(K, thresh);

% Plot the images and the corresponding key points found with the detectors

figure(1)
subplot(2,2,1)
imshow(Im, [])
plotMarks(map_K)
title('local maxima of K')

map_KmaH = findLocalMax(KmaH, thresh);
subplot(2,2,2)
imshow(Im, [])
plotMarks(map_KmaH)
title(['local maxima of K-a*H^2, a= ', num2str(alpha)])

map_K2 = findLocalMax(K2, thresh);
subplot(2,2,3)
imshow(Im2, [])
plotMarks(map_K2)
title('local maxima of K')

map_KmaH2 = findLocalMax(KmaH2, thresh);
subplot(2,2,4)
imshow(Im2, [])
plotMarks(map_KmaH2)
title(['local maxima of K-a*H^2, a= ', num2str(alpha)])

%suptitle('Key points detection using invariants of the structure tensor')
