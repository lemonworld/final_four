img = imread('ueb32.jpg');
figure(1);
imshow(img);

figure(2);
edgimg = edge(img,'Canny');
imshow(edgimg);

figure(3);
[fx, fy] = gradient(double(img));
subplot(1,2,1), imshow(fx, []);
subplot(1,2,2), imshow(fy, []);
