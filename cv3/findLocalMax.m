function map=findLocalMax(f,varargin)
%Search for local maxima.
%   map=findLocalMax(f) searches in the 2D array f for local maxima. A
%   binary map is returned. 1 entries identify the maxima.
%
%   map=findLocalMax(f,thresh) allows to additionally specify a threshold
%   (default: thresh=1e-6).

if nargin>1
    thresh=varargin{1};
else
    thresh=1e-6;
end

map = f > thresh + imdilate(f, [1 1 1; 1 0 1; 1 1 1]);

