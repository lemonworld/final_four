function [ K, KmaH] = keyPointsStructureTensor(image, alpha )
% Compute K and K-alpha*H^2 (KmaH) for a given image and alpha
% K and H are the invariants of the structure tensor J
% J = w * [I_x^2 I_xI_y; I_xI_y I_y^2], where I(x,y) is the image
% intensity, I_x, I_y are partial derivatives of I, * denotes convolution
% with the kernel w (see "Features" slides from the course)
[n, m] = size(image);
image = imgaussfilt(image);
% 1. compute the partial derivatives Ix and Iy
[I_x, I_y] = gradient(image);
% 2. compute the product images: Ix.^2, Iy.^2 and Ix*Iy
c1 = I_x.^2; 
c2 = I_x .* I_y;
c3 = I_y.^2;
% 3. filter the components of J with a Gaussian kernel of width w
c1 = imgaussfilt(c1, 'FilterSize', [15 15]);
c2 = imgaussfilt(c2, 'FilterSize', [15 15]);
c3 = imgaussfilt(c3, 'FilterSize', [15 15]);
% 4. compute K, H and K-alpha*H^2 (KmaH)
K = zeros(n, m);
H = zeros(n, m);
for i=1:n
    for j=1:m
        K(i,j) = det([c1(i,j) c2(i,j); c2(i,j) c3(i,j)]);
        H(i,j) = trace([c1(i,j) c2(i,j); c2(i,j) c3(i,j)]);
        KmaH(i,j) = K(i,j) - alpha * H(i,j) * H(i,j);
    end
end
end

