function plotMlpClassification(X, L, WH, W, beta)
% plot_mlp_classification(X, L, WH, W, beta)
%
% Visualizes the classification computed by a multilayer perceptron.
%
% X:    Data points
%       2xM matrix (the data points must be two-dimensional)
% L:    Class labels for the data points
%       Vector of length M containing values "1" or "-1"
% WH:   Weight matrix for hidden layer
%       3xH matrix, each column contains the threshold and the weights for one
%       neuron in the hidden layer
% W:    Weight vector for output neuron (length H+1, the first component is
%       the threshold, and the remaining components are the weights)
% beta: Parameter for sigmoid function
%
% Note: In all cases, the threshold is the first component of the weight
% vector.

[N, num_points]=size(X);
num_hidden=size(WH, 2);

% Classify points
Y=zeros(size(L));
for i=1:num_points
    x=X(:,i);
    v=sigmoid(WH'*[-1; x], beta);
    Y(i)=2*(sigmoid(W'*[-1; v], beta)>=0)-1;
end

clf

% Plot "real" classification
subplot(1, 2, 1);
plotData(gcf, X', L, []);
axis equal;
axis tight;

% Plot classification computed by MLP
subplot(1, 2, 2);
plotData(gcf, X', Y, []);
axis equal;
axis tight;

% Plot class lines for neurons in hidden layer
%kcol='rbkgrcmyk';
%for k=1:min(num_hidden,length(kcol)),
%    plotClassLine(gcf, WH(2:3,k), WH(1,k), [kcol(k) '-']);
%end
for k=1:num_hidden,
    plotPlane(gcf, WH(2:3,k), WH(1,k),'k-');
end
