% function [WH, W] = learnMLP(X, S, M, epsilon)
%
% Learns weight vectors and thresholds for a multilayer perceptron.
%
% INPUT ARGUMENTS
%
% X:        Data points
%           NxM-Matrix (each column contains an N-dimensional data point)
%
% S:        Class labels for the data points
%           Vector of length M containing values "1" or "-1"
%
% M:        Number of neurons in the hidden layer
%
% epsilon:  Learning rate
%
%
% OUTPUT ARGUMENTS
%
% WH:   Matrix comprising the extended weight vectors of hidden neurons
%       (N+1)xM matrix (M 'hidden' thresholds included in 1st row)
%
% W:    Extended weight vector of output neuron
%       (M+1)x1 vector
%
function [WH, W] = learnMLP(X, S, M, epsilon)

[N, L]=size(X);

% Parameter values
beta=2;          % Parameter for sigmoid function
max_epochs=1500; % Maximum number of epochs

% Initialize weight matrix for hidden layer
% (Number of dimensions plus one for the threshold x number of hidden
% neurons)
WH=randn(N+1, M);

% Initialize weight vector for output layer
% (length is number of hidden neurons plus one for the threshold)
W=randn(M+1, 1);

% Learn data points up to max_epochs times
for epoch=1:max_epochs
    had_error=0;
    fprintf('Epoch %d\n', epoch);

    % Learn all data points in sequence
    for i=1:L
        % get current data point x
        x=X(:,i);

        
        %%%%%%%%%%%%%
        % TODO: realize forward propagation, i.e. compute outputs of hidden
        % neurons (Mx1 vector: v) and output of output neuron (1x1 scalar: y)
        % Hint: Use your classifyMLP implementation
        %%%%%%%%%%%%%
        [y, v] = classifyMLP(x, WH, W, beta);

        
        % Determine desired output (i.e. class label) of current data point x
        s = S(i);
        
        % Was the data point classified wrongly?
        if s*y<0
            had_error=1;
            
            
            %%%%%%%%%%%%%
            % TODO: Insert backpropagation and weight update here
            % Hint: Don't forget the learning rate 'epsilon'
            %%%%%%%%%%%%%
            delta = (s-y)*(1-y*y);
            deltaj = delta * W(2:M+1) .* (1 - v .* v);
            W(2:M+1) = W(2:M+1) + epsilon*delta*v;
            WH(2:N+1, :) = WH(2:N+1, :) + epsilon*x * deltaj';
        end
    end

    % Terminate if all data points were classified correctly in this epoch
    if ~had_error
      fprintf('Terminated after %d epochs\n', epoch);
      plotMlpClassification(X, S, WH, W, beta);
      pause(0.02);
      break;
    elseif mod(epoch, 5)==0
      plotMlpClassification(X, S, WH, W, beta);
      pause(0.02);
    end
end