%
% function Y = sigmoid(X, beta)
%
% Applies the sigmoid function to each element x in X. Output Y will have
% the same size as X, where each element y = tanh(.5*beta.*x).
%
% X:    Input matrix, vector or scalar
%
% beta: Real valued scalar (beta >= 0). beta = inf is allowed.
%
function Y = sigmoid(X, beta)

if isinf(beta)
    Y=ones(size(X));
    Y(find(X<0))=-1;
else
    Y=tanh(.5*beta.*X);
end