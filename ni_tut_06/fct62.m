clear;
close all;

% load weights
load('ueb62.mat');

L = 10000;
% uniformly create 2D random data in [-1,1]x[-1,1] (2xL matrix)
X = rand([2, L])*2 - 1;

% define vector of different beta values (sigmoid function of the MLP)
betaVec = [3, 5, 20, 1000];

% show classification for each beta in betaVec
for iBeta=1:length(betaVec)
    
    % load beta of current index iBeta from betaVec
    beta = betaVec(iBeta);
    
    % call classifyMLP to classify X corresponding to weights WH and W
    [Y, ~] = classifyMLP(X, WH, W, beta);
    
    % assign each output value >= 0 in Y the class prediction +1 and
    % each output value in Y < 0 the class prediction -1
    Y = (Y>=0) * 2-1;
    
    % plot the classified classes
    h=iBeta;
    plotData(h, X', Y', []);
    hold on;

    % visualize linear classification boundary of each hidden neuron
    for i=1:size(WH,2)
        theta = WH(1, i);
        w = WH(2:3, i);
        plotPlane(h,w,theta,'b-')
    end
    
    titleText = sprintf('beta = %.2f', beta);
    title(titleText);
end