%
% function [Y, V] = classifyMLP(X, WH, W, beta)
%
% Computes the output of a multilayer perceptron (MLP) that has one hidden
% layer (comprising M hidden neurons) and a single output neuron.
% The threshold of each neuron is assumed to be in the first component of
% its extended weight vector.
%
% X:    Data matrix
%       NxL matrix (each column contains an N-dimensional data point)
%
% WH:   Matrix comprising the extended weight vectors of hidden neurons
%       (N+1)xM matrix (M 'hidden' thresholds included in 1st row)
%
% W:    Extended weight vector of output neuron
%       (M+1)x1 vector
%
% beta: Parameter controlling the slope of sigmoid function (sigmoid.m)
%       scalar
%
% Y:    MLP output of data points X
%       1xL vector
%
% V:    outputs of hidden neurons for all inputs
%       MxL matrix
function [Y, V] = classifyMLP(X, WH, W, beta)

[N, L] = size(X);
[~, M] = size(WH);

assert(isequal(size(WH), [N+1, M]), ...
    'expected argument WH to be a (N+1)xM matrix');
assert(isequal(size(W), [M+1, 1]), ...
    'expected argument W to be a (M+1)x1 vector');
assert(isscalar(beta), ...
    'expected argument beta to be a scalar');

% extend data matrix X with '-1' dimension as first component
X = [-1*ones(1, L); X];

assert(isequal(size(X), [N+1, L]), ...
    'expected extended matrix X to be a (N+1)xL matrix');

% compute outputs of hidden neurons for all extended data points
% (Hint: afterwards V should be a MxL matrix)
V = sigmoid(WH'*X, beta);

assert(isequal(size(V), [M, L]), ...
    'expected matrix V to be a MxL matrix');

% extend V with '-1' dimension as first component
% (Hint: afterwards V should be a (M+1)xL matrix)
V = [-1 * ones(1,L); V];

assert(isequal(size(V), [M+1, L]), ...
    'expected extended matrix V to be a MxL matrix');

% compute output of output neuron
% (Hint: afterwards Y should be a 1xL vector)
Y = sigmoid(W'*V, beta);

% The first row of V is deleted, i.e. the '-1' dimension is discarded for
% output
V = V(2:end, :);