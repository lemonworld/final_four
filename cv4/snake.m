function contour = snake(img, contour, options)
% function snake(img, contour, options)
%
% Main loop for greedy snake algorithm.
% img:        Input image 
% contour:    Initialization of snake
% options:    Parameters of algorithm
    
    % initialization of parameters
    alpha = options.alpha * ones(1, size(contour, 2));
    beta  = options.beta  * ones(1, size(contour, 2));
    gamma = options.gamma * ones(1, size(contour, 2));
    
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Aufgabe 4.1.1: Kantendetektion
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    norm_image_gradient = zeros(size(img));
    [gx, gy] = gradient(img);
    [m, n] = size(img);
    for i = 1:m
        for j = 1:n
            norm_image_gradient(i,j) = norm([gx(i,j), gy(i,j)]);
        end
    end
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % display initialization of snake
    figure
    imshow(img, [])
    hold on
    plot([contour(2,:) contour(2,1)], [contour(1,:) contour(1,1)], 'r.-', ...
         'MarkerSize', 18)
    plot_title = sprintf('iteration %d', 0);
    title(plot_title)
    drawnow
    
    k = size(contour, 2);
    k_updated = k;
    iterations = 0;
    % iterate until snake stops changing
    while k_updated > k * options.contour_fraction
        iterations = iterations + 1;
        disp(iterations);
        k_updated = 0;
        % update all contour points
        for i = 1:k
            [p, updated] = greedy_minimization(norm_image_gradient, contour, ...
                                               i, alpha, beta, gamma, options);
            % eliminate corners if required
            contour(:,i) = p;
            if iterations > options.begin_corner_elim
                [beta] = corner_elimination(contour, beta, options);
            end
            % check if a contour points has been updated
            if updated
                k_updated = k_updated + 1;
            end
        end
        
        % display current state of snake
        pause(0.02);
        imshow(img, [])
        hold on
        plot([contour(2,:) contour(2,1)], [contour(1,:) contour(1,1)], 'r.-', ...
             'MarkerSize', 18)
        plot_title = sprintf('iteration %d', iterations);
        title(plot_title)
        drawnow
    end
    
function [new_p_i, updated] = greedy_minimization(norm_image_gradient, ...
                                                  contour, i, alpha, beta, ...
                                                  gamma, options)
    [m, n] = size(norm_image_gradient);
    k = size(contour, 2);
    w = (options.neighborhood_size - 1) / 2;
    d = average_distance(contour);
    updated = false;
    
    % get previous, current, and next point in snake
    p_iminus1 = contour(:, wrap_index(i-1, k));
    p_i       = contour(:,i);
    p_iplus1  = contour(:, wrap_index(i+1, k));
    
    % compute individual terms of energy functional
    p = [];
    E_cont = [];
    E_curv = [];
    E_imag = [];
    for p_i_x = max(1, p_i(1)-w):min(p_i(1)+w, m)
        for p_i_y = max(1, p_i(2)-w):min(p_i(2)+w, n)
            % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Aufgabe 4.1.2.: Energieterme berechnen
            % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            npi = [p_i_x; p_i_y];
            e_cont = 0.5 * (norm(npi - p_iminus1) - norm(npi-p_iplus1))^2;
            e_curv = norm(p_iminus1 - 2*npi  + p_iplus1) ^ 2;
            e_imag = -norm_image_gradient(p_i_x, p_i_y);

            % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % save current point and corresponding energy terms for
            % entire neighborhood
            p = [p [p_i_x p_i_y]'];
            E_cont = [E_cont e_cont];
            E_curv = [E_curv e_curv];
            E_imag = [E_imag e_imag];
        end
    end

    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Aufgabe 4.1.3: Energieterme normieren
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if(max(E_cont) ~= min(E_cont))
        E_cont = (E_cont - min(E_cont)) / (max(E_cont) -  min(E_cont));
    end
    if(max(E_curv) ~= min(E_curv))
        E_curv = (E_curv - min(E_curv)) / (max(E_curv) - min(E_curv));
    end
    if(max(E_imag) ~= min(E_imag))
        E_imag = (E_imag - min(E_imag)) / (max(E_imag) - min(E_imag));
    end
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % combine individual energy terms
    EPS = alpha(i) * E_cont + beta(i)  * E_curv + gamma(i) * E_imag;

    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Aufgabe 4.1.4: new_p_i wird Punkt mit minimaler Energie
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [~, ind] = min(EPS);
    new_p_i = p(:, ind);
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % determine if current point of snake was moved

    if p_i ~= new_p_i
        updated = true;
    end
    
function beta = corner_elimination(contour, beta, options)
    n = size(contour, 2);
    K = zeros(1, n);
    % compute curvature of snake at each point
    for i = 1:n
        % get previous, current, and next point in snake
        p_iminus1 = contour(:, wrap_index(i-1, n));
        p_i       = contour(:,i);
        p_iplus1  = contour(:, wrap_index(i+1, n));
        K(i) = sum((p_iminus1 - 2*p_i + p_iplus1).^2);
    end
    
    % determine local maxima and set beta to zero there
    for i = 1:n
        if K(wrap_index(i-1, n)) < K(i) & ...
           K(i) > K(wrap_index(i+1, n)) & ...
           K(i) > options.K_threshold
            beta(i) = 0;
        end
    end        
    
function avg_dist = average_distance(contour)
    avg_dist = 0;
    n = size(contour, 2);
    for i = 1:(n-1)
        avg_dist = avg_dist+norm(contour(:,i) - contour(:,i+1));
    end
    avg_dist = avg_dist+norm(contour(:,n) - contour(:,1));
    avg_dist = avg_dist / n;
    
function index = wrap_index(index, k)
    if index < 1
        index = k;
    elseif index > k
        index = 1;
    end
