function contour = set_contour(file, N)
    load(file)
    
    imshow(img)
    drawnow
    hold on
    
    contour = zeros(2, N);
    for i = 1:N
        [y,x] = ginput(1);
        contour(:,i) = [x y]';
        
        plot(y, x, 'r.')
        title(i)
    end
    