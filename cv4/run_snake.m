function run_snake(which_img)
% function run_snake(which_img)
%
% Starts greedy snake for specified image.
% which_img:   Index of image to be used ({1, 2, 3})
    
    % initialization of parameters
    options.neighborhood_size = 7;
    options.contour_fraction = 0.1;
    options.alpha = 1;
    options.beta  = 1;
    options.gamma = 1;
    options.begin_corner_elim = 0;
    options.K_threshold = inf;
    
    % simple blob
    if which_img == 1
        load 'ueb421.mat';
    % simple square
    elseif which_img == 2
        load 'ueb422.mat';
    % coffee mug
    elseif which_img == 3
        load 'ueb423.mat';
    end    

    % display results
    figure
    subplot(1, 2, 1)
    imshow(img, [])
    hold on
    plot([contour(2,:) contour(2,1)], [contour(1,:) contour(1,1)], 'r.-', ...
         'MarkerSize', 18)

    contour = snake(img, contour, options);
            
    figure(1)
    subplot(1, 2, 2)
    imshow(img, [])
    hold on
    plot([contour(2,:) contour(2,1)], [contour(1,:) contour(1,1)], 'r.-', ...
         'MarkerSize', 18)
    
 
