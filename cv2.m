load('~/exercises/ueb24.mat')
f = 0.008;
ps = 0.00004;
[m, n] = size(B);
X = zeros(m*n, 1);
Y = zeros(m*n, 1);
Z = zeros(m*n, 1);
for i = 1:m
    for j = 1:n
        r = B(i, j);
        ind = (i-1)*n + j;
        p = [(i - 72.5)*ps (j - 88.5)*ps f];
        p = r / norm(p) * p;
        X(ind) = p(1);
        Y(ind) = p(2);
        Z(ind) = p(3);
    end
end
scatter3(X, Z, Y);