function W = kMeansCluster(X, k, numEpochs, epsilon_s, epsilon_e)
%
%   function W = kMeansCluster(X, k, numEpochs,lambda_s, lambda_e)
%
%   k-Means algorithm
%
%   input:
%       X:          data points (nxd matrix, n points with d dimensions)
%       k:          number of codebook vectors
%       numEpochs:  max. number of training epochs
%       epsilon_s:  initial learning rate
%       epsilon_e:  final learning rate
%
%	output:
%       W:          codebook
%

% choose a plotMode 
%   1 - for samplewise plotting [slow but useful for debugging or to see
%       hard-competitive learning fashion]
%   2 - for epochwise plotting  [faster visualization, 1 plot per epoch]
plotMode = 2;

% choose delay time in seconds after each single plot (e.g. 0.5)
pauseTime = 0.2;

[n,d] = size(X);

% randomly initialize codebook
W = 10 * rand(k,d) -5;

for t=0:numEpochs
    
    % set the current learning rate epsilon_t
    epsilon_t = epsilon_s*(epsilon_e/epsilon_s)^(t/numEpochs);
    
    fprintf('starting epoch t=%.4d [epsilon_t=%.4f]...', t, epsilon_t);
    
    % iteration over data points (in random order)
    for mu = randperm(n)
        x_mu = X(mu,:);
        
        % plot current codebook and x_mu if samplewise plotting is selected
        if plotMode == 1
            plotCodebook(X, W, pauseTime, x_mu);
        end
        
        
        % implement the k-means learning rule
        %
        % HINT: if you want to avoid a loop over the codebook vectors when 
        %       computing the distance of x_mu to each codebook vector
        %       you could use the command 'dist' (help dist)
        
       [~, istar] = min(cellfun(@norm, num2cell(W - x_mu, 2))); 
       W(istar, :) = W(istar, :) + epsilon_t*(x_mu - W(istar, :));
        % plot new codebook and x_mu if samplewise plotting is selected
        if plotMode == 1
            plotCodebook(X, W, pauseTime, x_mu);
        end
    end
    
    % plot current codebook if epochwise plotting is selected
    if plotMode == 2
        plotCodebook(X, W, pauseTime);
    end
    
    fprintf('finished.\n');
end