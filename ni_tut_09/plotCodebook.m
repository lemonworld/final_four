function plotCodebook(X, W, pauseTime, varargin)
%
% function plotCodebook(X, W, pauseTime)
% function plotCodebook(X, W, pauseTime, x)
%
% plots data points X colored according to codebook affiliation
%
% input parameters:
%   X:  data points (nxd-matrix, n points, d dimensions)
%   W:  codebook (kxd-matrix, k codebook vectors, d dimensions)
%
% optional:
%   x:  single data point to highlight
%
if nargin<4
    x=[];
else
    x=varargin{1};
    assert(length(x) == 2);
end

markerSize = 10;

% get data dimensionality
d = size(X, 2);
assert(d==2);

% get the number of codebook vectors
k=size(W,1);

% define colors associated with codebook vectors
colmap=jet(k);

% for each data point in X compute closest codebook vector
distMat = dist(X, W');
[~, class]=min(distMat,[],2);

% start plotting
figure(1);
clf;

% plot all data points in their class color
for i=1:k
    ind=(class==i);
    plot(X(ind,1), X(ind,2),'.','MarkerEdgecolor',colmap(i,:));
    hold on;
end

% plot codebook vectors
plot(W(:,1),W(:,2), 'o', 'MarkerSize', markerSize, ...
    'MarkerFaceColor',[.3 .3 .3], 'MarkerEdgeColor', [0 0 0]);

if ~isempty(x)
    % plot optional single data point x
    plot(x(1), x(2), 'o', 'MarkerSize', round(.75*markerSize), ...
        'MarkerFaceColor', [1 0 0], 'MarkerEdgeColor', [0 0 0]);
    hold off;
end

axis equal;
axis tight;
hold off;

pause(pauseTime);