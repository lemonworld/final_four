function W = neuralGas(X, k, numEpochs, epsilon_s, epsilon_e, ...
    lambda_s, lambda_e)
%
%   function W = neuralGas(X, k, numEpochs, epsilon_s, epsilon_e, ...
%       lambda_s, lambda_e)
%
%   Neural Gas algorithm
%
%   input:
%       X:          data points (nxd matrix, n points with d dimensions)
%       k:          number of codebook vectors
%       numEpochs:  max. number of training epochs
%       epsilon_s:  initial learning rate
%       epsilon_e:  final learning rate
%       lambda_s:   initial neighborhood radius
%       lambda_e:   final neighborhood radius
%
%	output:
%       W:          codebook
%

% choose a plotMode 
%   1 - for samplewise plotting [slow but useful for debugging or to see
%       soft-competitive learning fashion]
%   2 - for epochwise plotting  [faster visualization, 1 plot per epoch]
plotMode = 2;

% choose delay time in seconds after each single plot (e.g. 0.5)
pauseTime = 0.2;

[n,d] = size(X);

% randomly initialize codebook
W = 10 * rand(k,d) -5;

for t=0:numEpochs
    
    % set the current learning rate epsilon_t and the current 
    % neighborhood radius lambda_t
    epsilon_t = epsilon_s*(epsilon_e/epsilon_s)^(t/numEpochs);
    lambda_t = lambda_s*(lambda_e/lambda_s)^(t/numEpochs);
    
    fprintf('starting epoch t=%.4d [epsilon_t=%.4f, lambda_t=%.4f]...', ...
        t, epsilon_t, lambda_t);
    
    % iteration over data points (in random order)
    for mu = randperm(n)
        x_mu = X(mu,:);
        
        % plot current codebook and x_mu if samplewise plotting is selected
        if plotMode == 1
            plotCodebook(X, W, pauseTime, x_mu);
        end
        
        
        % TODO: implement the Neural Gas learning rule
        %
        % HINT: if you want to avoid a loop over the codebook vectors when 
        %       computing the distance of x_mu to each codebook vector
        %       you could use the command 'dist' (help dist)
        %
        %       the command 'sort' might help you to determine the rank of
        %       the codebook vectors and to accordingly select the closest,
        %       2nd closest, etc. codebook vector (help sort)
        %
        %       take care of correclty indexing the codebook vectors
        
        ds = cellfun(@norm, num2cell(W - x_mu, 2));
        [ds, I] = sort(ds);
        for i = 1:k
            W(I(i),:) = W(I(i),:) + epsilon_t * exp(-(i-1)/lambda_t) * (x_mu ...
                                                              - W(I(i),:));
        end
        % plot new codebook and x_mu if samplewise plotting is selected
        if plotMode == 1
            plotCodebook(X, W, pauseTime, x_mu);
        end
    end
    
    % plot current codebook if epochwise plotting is selected
    if plotMode == 2
        plotCodebook(X, W, pauseTime);
    end
    
    fprintf('finished.\n');
end