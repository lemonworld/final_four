[Y1, ~] = audioread('wav/mixture01.wav');
[Y2, ~] = audioread('wav/mixture02.wav');
[Y3, ~] = audioread('wav/mixture03.wav');
[Y4, ~] = audioread('wav/mixture04.wav');
[Y5, ~] = audioread('wav/mixture05.wav');
Y = [Y1'; Y2'; Y3'; Y4'; Y5'];
[newY, A, W] = fastica(Y);
res = W*Y;

soundsc(res(1,:), 44100)
pause(10)
soundsc(res(2,:), 44100)
pause(10)
soundsc(res(3,:), 44100)
pause(10)
soundsc(res(4,:), 44100)
pause(10)
soundsc(res(5,:), 44100)
