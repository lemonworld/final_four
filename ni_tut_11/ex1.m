load('ueb11-1.mat')
plotjoint(X)
pause(1);
X = X - mean(X,2);
[newX, V, me, l] = pcaproj(X', 2);
newX = newX';
plotjoint(newX)
pause(1);
newX = inv(diag(sqrt(l))) * newX;
plotjoint(newX)
pause(1);
mx_angle = 0;
mx = 0;
for i = 0:0.01:2*pi
    A = [cos(i) -sin(i); sin(i) cos(i)];
    res = abs(0.5*sum(kurtosis(A'*newX, 1, 2))-3);
    if res > mx
        mx = res;
        mx_angle = i;
    end
end
A = [cos(mx_angle) -sin(mx_angle); sin(mx_angle) cos(mx_angle)];
nnewX = A'*newX;
plotjoint(nnewX)