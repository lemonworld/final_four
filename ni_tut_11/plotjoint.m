function plotjoint(X)
% plotjoint  Plots joint distribution and marginal distributions of 2D signal
%
% Input:
%     X    Data matrix containing n two-dimensional column vectors

    % Plot joint distribution
    subplot(2, 2, 2);
    h1=gca;
    plot(X(1,:), X(2,:), '.');
    axis equal;
    a=axis;

    % Generate histograms for marginal distributions
    [n1,ctr1] = hist(X(1,:),100);
    [n2,ctr2] = hist(X(2,:),100);

    % Plot marginal distribution for first component
    subplot(2, 2, 4);
    h2=gca;
    bar(ctr1, -n1, 1); 
    axis([a(1) a(2) -max(n1)*1.1 0]);
    axis('off');

    % Plot marginal distribution for second component
    subplot(2, 2, 1);
    h3=gca;
    barh(ctr2,-n2,1);
    axis([-max(n2)*1.1 0 a(3) a(4)]);
    axis('off');
    
    subplot(2, 2, 2);
    % Adjust positions of axes
    set(h1,'Position',[0.35 0.35 0.55 0.55]);
    set(h2,'Position',[.35 .1 .55 .15]);
    set(h3,'Position',[.1 .35 .15 .55]);
