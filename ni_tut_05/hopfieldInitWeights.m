function W=hopfieldInitWeights(patterns)
% Initialize weights for Hopfield net.
%
% Input:
% patterns      Cell array of patterns. Each pattern is an image, i.e. a
%               matrix; to convert the pattern to a vector, use the notation
%               'pattern{j}(:)'.
%
% Output:
% W             Weight matrix, size NxN, where N is the total number of
%               elements (pixels) in each pattern.

num_patterns = length(patterns);
num_inputs = numel(patterns{1});
W = zeros(num_inputs, num_inputs);
for j = 1:num_patterns
    rmt = patterns{j}(:);
    W = W + 1/num_inputs * rmt * rmt';
end
W(1:num_inputs+1:end) = zeros(1, num_inputs);
