function activation=hopfieldAssociate(W, activation)
% Hopfield activation rule.
%
% Inputs:
% W             weight matrix (size NxN)
% activation    Initial activation of the neurons. This can either be a vector
%               of length N or an image (matrix) with a total of N pixels. In
%               either case, 'activation(:)' can be used to obtain an
%               activation vector of length N.
%
% Output:
% activation    Final activation of the neurons after convergence

% Value output by neurons when they are not firing (i.e. "off")
offState = -1;

% Threshold for neurons
threshold = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implement the Hopfield activation rule
% You can use the function plotActivation() to plot the new activation
% of the net after each update
activation = activation(:);
while true
    old_activation = activation;
    N = length(activation);
    p = randperm(N);
    for j = 1:N
        activation(p(j)) = (W(p(j),:)*activation > 0) * 2 - 1;
    end
    if prod(activation == old_activation) == 1
        break
    end
end
activation = reshape(activation, [sqrt(N), sqrt(N)]);