function hopfield(patterns, noise_level)
% Implements a Hopfield net.
%
% Inputs:
% patterns      Cell array of patterns (images)
% noise_level   Amount of noise to add to the patterns for testing, given as a
%               fraction of the number of pixels, i.e. noise_level=0.1 flips
%               ten percent of the pixels.

num_patterns=length(patterns);
num_inputs=numel(patterns{1});

% Initialize weights
W=hopfieldInitWeights(patterns);

for j=1:num_patterns
    fprintf('Trying to associate pattern %d\n', j);
    
    % Plot original pattern
    subplot(1,3,1);
    plotActivation(patterns{j});
    title('original pattern');
    
    % Add noise to the pattern
    noise=find(rand(num_inputs,1) < noise_level);
    activation=patterns{j};
    activation(noise)=-activation(noise);
    
    % Plot noisy pattern
    subplot(1,3,2);
    plotActivation(activation);
    title('pattern with noise');
    
    % Try to recognize pattern
    subplot(1,3,3);
    activation=hopfieldAssociate(W, activation);
    plotActivation(activation);
    title('hopfield fixpoint')
    pause(1);
end
