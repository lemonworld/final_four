function handle = plotActivation(activation)
% Displays the current activation of a Hopfield net.

    inputWidth=size(activation);
    activationGrid = zeros(inputWidth+1);
    colormap(winter);
    activationGrid(inputWidth(1):-1:1,1:inputWidth(2)) = activation;
    handle=pcolor(activationGrid);
    axis equal;
    axis tight;
    axis off;
