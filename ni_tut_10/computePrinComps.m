function [W, l] = computePrinComps(X)
C = cov(X);
[W, l] = eig(C);
l = diag(l);
[l, ind] = sort(l, 'descend');
W = W(:, ind);
end
