clear;
close all;

% load a natural image
% select an image file
fileName = 'berge.jpg';

img = im2double(imread(fileName));

imgHeight = size(img, 1);
imgWidth = size(img, 2);

% get single r-/g-/b-channel images
imgR = img(:, :, 1);
imgG = img(:, :, 2);
imgB = img(:, :, 3);

% display color-channels
figure(1);
colormap('gray');

subplot(3,3,1);
imagesc(imgR);
axis equal; axis tight;
colorbar;
title('R-channel');

subplot(3,3,2);
imagesc(imgG);
axis equal; axis tight;
colorbar;
title('G-channel');

subplot(3,3,3);
imagesc(imgB);
axis equal; axis tight;
colorbar;
title('B-channel');

% create data matrix P of RGB pxiel values
P = [imgR(:)'; imgG(:)'; imgB(:)'];

% create data matrix P0 of mean subtracted RGB pxiel values P
P0 = P - mean(P, 2);

%  perform PCA transformation of P0
[W, ~] =  computePrinComps(P0');
pcaP =  inv(W) * P0;

% display PCA channels
subplot(3,3,4);
imagesc(reshape(pcaP(1,:), imgHeight, imgWidth));
axis equal; axis tight;
colorbar;
title('PCA-channel #1');

subplot(3,3,5);
imagesc(reshape(pcaP(2,:), imgHeight, imgWidth));
axis equal; axis tight;
colorbar;
title('PCA-channel #2');

subplot(3,3,6);
imagesc(reshape(pcaP(3,:), imgHeight, imgWidth));
axis equal; axis tight;
colorbar;
title('PCA-channel #3');

% perform RGB->YUV transformation of P
T = [0.299 0.587 0.114; -0.14713 -0.28886 0.436; 0.615 -0.51499 -0.10001];
yuvP = T*P;

% display YUV channels
subplot(3,3,7);
imagesc(reshape(yuvP(1,:), imgHeight, imgWidth));
axis equal; axis tight;
colorbar;
title('Y-channel');

subplot(3,3,8);
imagesc(reshape(yuvP(2,:), imgHeight, imgWidth));
axis equal; axis tight;
colorbar;
title('U-channel');

subplot(3,3,9);
imagesc(reshape(yuvP(3,:), imgHeight, imgWidth));
axis equal; axis tight;
colorbar;
title('V-channel');