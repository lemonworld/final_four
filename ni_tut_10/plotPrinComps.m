%
% function plotPrinComps(W, X)
%
% plots the direction of principal components W corresponding to a 2D data
% matrix X
%
% W:    2x2 matrix - PCs: each column represents 1 principal component
%
% X:    2xL matrix - data matrix
%
function plotPrinComps(W, X)

assert(size(X, 1) == 2, 'this function can only by applied for 2D data');
assert(isequal(size(W), [2, 2]), 'this function can only by applied for 2D data');

clf;
hold off;

plot(X(1,:), X(2,:), 'b.');
hold on;
axis equal;
mu_X = mean(X, 2);

newX = X - mu_X;
newX = inv(W)*newX;
plot(newX(1,:), newX(2,:), '.');

for i = 1:2
    line([mu_X(1), mu_X(1)+W(1, i)], [mu_X(2), mu_X(2)+W(2,i)/i], ...
        'Color', [1 1 0], 'LineWidth', 2, 'linestyle',':');
end
hold off;